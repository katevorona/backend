'use strict';

const bunyan = require('bunyan');
const fs = require('fs');
const { JSDOM } = require('jsdom');

const logger = bunyan.createLogger({ name: "myapp" });

const targetElementId = "make-everything-ok-button";
const PERCENTAGE_OF_ATTRIBUTE_MATCHING = 50;

const INDEX_FIRST_FILE = 2;
const INDEX_SECOND_FILE = 3;

try {
  const sampleFile = getFile(INDEX_FIRST_FILE);
  const anotherFile = getFile(INDEX_SECOND_FILE);
  const domFirstFile = new JSDOM(sampleFile);
  const domSecondFile = new JSDOM(anotherFile);

  const button = domFirstFile.window.document.getElementById(targetElementId);
  const attributesArray = Array.prototype.slice.apply(button.attributes);

  const accommodationOfAttributes = getAttributesArray(attributesArray);

  const similarElement = getSimilarElement(accommodationOfAttributes, domSecondFile, button);
  if(!similarElement) {
    logger.error('Similar element was not found');
    return;
  }

  logger.info(`Successfully found the similar element. Element Text: ${similarElement.textContent}`);
  const parentThree = getParentThree(similarElement)
  logger.info(`Element three: ${parentThree.join('->')}`);
} catch (err) {
  logger.error('Error trying to find element by css selector', err);
}

function getFile(index) {
  return fs.readFileSync(process.argv[index])
}

function getAttributesArray(attributesArray) {
  const attrArrNameValue = attributesArray.map(attr => `[${attr.name}="${attr.value}"]`)

  return accommodation(attrArrNameValue)
}

function accommodation(attributesArr){
  let accommodationArr = [attributesArr]
  const countOfElem = Math.round(attributesArr.length*PERCENTAGE_OF_ATTRIBUTE_MATCHING/100)

  const countSliced = attributesArr.length - countOfElem

  let iteration = 0

  while (iteration < countSliced) {
    const resultArr = []
    for(let j=0; j< accommodationArr.length; j++) {
      for(let i=0; i < accommodationArr[j].length; i++){
        const splicedArr = Array.from(accommodationArr[j])
        splicedArr.splice(i, 1)
        const indexOfTheSameElement = resultArr.findIndex(elem => elem.join('') === splicedArr.join(''))
        if (indexOfTheSameElement === -1) {
          resultArr.push(splicedArr)
        }
      }
    }
    accommodationArr = resultArr
    iteration++
  }

  return accommodationArr
}

function getSimilarElement(accommodationOfAttributes, domSecondFile, sourceElement) {
  const findElements = []

  accommodationOfAttributes.forEach(attributes => {
    const findElem = domSecondFile.window.document.querySelector(attributes.join(''))
    if(findElem) {
      findElements.push(findElem)
    }
  })

  if(findElements.length === 0) {
    return null
  }

  if(findElements.length > 1) {
    const index = findElements.findIndex(element => element.textContent === sourceElement.textContent)
    if (index !== -1) {
      return findElements[index]
    }
  }

  return findElements[0]
}


function getParentThree(element) {
  const parentThree = []

  findParent(element)

  function findParent(element) {
    const parent = element.parentNode;  //find parent
    const parentChildren = Array.from(parent.childNodes); //transform object to array
    const withTheSameNode = parentChildren.filter(child => child.nodeName === element.nodeName); //find all children of array with element tag

    if(withTheSameNode.length !== 1) {
      const index = withTheSameNode.findIndex(child => child === element)
      parentThree.pop()
      parentThree.push(`${element.nodeName}[${index}]`)
    }

    parentThree.push(parent.nodeName)

    if(parent.nodeName !== 'HTML') { //the top tag
      findParent(parent)
    }
  }

  return parentThree.reverse()
}


