# README #

## Install all dependencies
Run `npm install`

## Run application
`node index.js {PATH_TO_FILE_ONE} {PATH_TO_FILE_TWO}`

##Examples
1) First and second file
`node index.js ./samples/sample-0-origin.html ./samples/sample-1-evil-gemini.html`

Found button with id 'make-everything-ok-button' in first file and found similar button in the second file. Result:
`Element three: HTML->BODY->DIV->DIV->DIV[2]->DIV[0]->DIV->DIV[1]->A[1]`

2) First and third file
`node index.js ./samples/sample-0-origin.html ./samples/sample-2-container-and-clone.html`

Found button with id 'make-everything-ok-button' in first file and found similar button in the third file. 
Result:
`Element three: HTML->BODY->DIV->DIV->DIV[2]->DIV[0]->DIV->DIV[1]->DIV`

3) First and fourth file
`node index.js ./samples/sample-0-origin.html ./samples/sample-3-the-escape.html`
Result:
`Element three: HTML->BODY->DIV->DIV->DIV[2]->DIV[0]->DIV->DIV[2]`

4) First and fifth file
`node index.js ./samples/sample-0-origin.html ./samples/sample-4-the-mash.html`
Result:
`Element three: HTML->BODY->DIV->DIV->DIV[2]->DIV[0]->DIV->DIV[2]`

